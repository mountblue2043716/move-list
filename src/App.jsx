import React from "react";
import ListsContainer from "./components/ListsContainer";

const App = () => {
  return (
  <div>
    <ListsContainer />
  </div>
  )
};

export default App;
