import React from "react";
import List from "./List";
import ButtonsContainer from "./ButtonsContainer";
import { useState } from "react";

let list1 = {
  0: { id: 0, name: "HTML", checked: false },
  1: { id: 1, name: "CSS", checked: false },
  2: { id: 2, name: "JS", checked: false },
};
let list2 = {
  3: { id: 3, name: "React", checked: false },
  4: { id: 4, name: "Vue", checked: false },
  5: { id: 5, name: "Angular", checked: false },
};

const ListsContainer = () => {
  const [list1Items, setList1Items] = useState(list1);
  const [list2Items, setList2Items] = useState(list2);

  function handleCheckChange(item) {
    if (list1Items[item.id]) {
      setList1Items({
        ...list1Items,
        [item.id]: {
          ...list1Items[item.id],
          checked: !item.checked,
        },
      });
    }
    if (list2Items[item.id]) {
      setList2Items({
        ...list2Items,
        [item.id]: {
          ...list2Items[item.id],
          checked: !item.checked,
        },
      });
    }
  }

  function moveRight() {
    let newList1 = {},
      newList2 = {};
    for (let id in list1Items) {
      if (list1Items[id].checked) {
        newList2[id] = {
          ...list1Items[id],
          checked: false,
        };
      } else newList1[id] = list1Items[id];
    }
    setList1Items(newList1);
    setList2Items({ ...newList2, ...list2Items });
  }

  function moveLeft() {
    let newList1 = {},
      newList2 = {};
    for (let id in list2Items) {
      if (list2Items[id].checked) {
        newList1[id] = {
          ...list2Items[id],
          checked: false,
        };
      } else newList2[id] = list2Items[id];
    }
    setList2Items(newList2);
    setList1Items({ ...newList1, ...list1Items });
  }
  function moveAllRight() {
    let newList1 = {};
    for (let id in list1Items) {
          newList1[id] = {
          ...list1Items[id],
          checked: false,
        };
    }
    setList2Items({ ...list2Items, ...newList1 });
    setList1Items({});
  }

  function moveAllLeft() {
    let newList2 = {};
    for (let id in list2Items) {
          newList2[id] = {
          ...list2Items[id],
          checked: false,
        };
    }
    setList1Items({ ...list1Items, ...newList2 });
    setList2Items({});
  }

  return (
    <div className="main-container">
      <List list={list1Items} onCheckChange={handleCheckChange} />
      <ButtonsContainer
        moveRight={moveRight}
        moveAllRight={moveAllRight}
        moveLeft={moveLeft}
        moveAllLeft={moveAllLeft}
      />
      <List list={list2Items} onCheckChange={handleCheckChange} />
    </div>
  );
};

export default ListsContainer;
