import React from "react";

const List = ({ list, onCheckChange }) => {
  return (
    <div className="list1-container">
      <form>
        {Object.values(list).map((item) => {
          return (
            <li key={item.id}>
              <label>
                <input type="checkbox"
                onChange={(e) => onCheckChange(item)}
                />
                {item.name}
              </label>
            </li>
          );
        })}
      </form>
    </div>
  );
};

export default List;
