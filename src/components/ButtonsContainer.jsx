import React from 'react'

const ButtonsContainer = ( {moveRight, moveAllRight, moveLeft, moveAllLeft} ) => {
  return (
    <div className='buttons-container'>
      <button onClick={moveRight}> MOVE RIGHT</button>
      <button onClick={moveAllRight}>MOVE ALL RIGHT</button>
      <button onClick={moveLeft}>MOVE LEFT</button>
      <button onClick={moveAllLeft}>MOVE ALL LEFT</button>
    </div>
  )
}

export default ButtonsContainer
